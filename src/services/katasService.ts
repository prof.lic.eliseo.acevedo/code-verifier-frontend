import { AxiosRequestConfig } from 'axios';
import axios from '../utils/config/axios.config';

/**
   * Obtains all katas.
   * @param {string} token - Authentication token.
   * @param {number} [limit] - Limit of results per page.
   * @param {number} [page] - Page number.
   * @returns {Promise<any>} - Promise with the kata data.
   */
export const getAllKatas = (token:string, limit?: number, page?: number) => {

    // http://localhost:8000/api/katas?limit=1&page=1
    // Add headers with JWT in x-access-token
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            limit,
            page
        }
    }
    return axios.get('/katas', options)

}

/**
   * Obtains a kata for his ID.
   * @param {string} token - Authentication token.
   * @param {string} id - ID of the kata.
   * @returns {Promise<any>} - Promise with the kata data.
   */
export const getKataByID = (token: string, id: string) => {
    // http://localhost:8000/api/katas?id=XXXXXXXXXXXX
    // Add headers with JWT in x-access-token
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id
        }
    }

    return axios.get('/katas', options)
}


/**
   * Create a new kata.
   * @param {string} token - Authentication token.
   * @param {Object} kataData - Data of the kata to create.
   * @returns {Promise<any>} - Promise with the data of the created kata.
   */
export const createKata = (token: string, data: object) => {
    // http://localhost:8000/api/katas
    // Add headers with JWT in x-access-token
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        }
    }

    return axios.post('/katas', data, options)
}

/**
   * Update an existing kata.
   * @param {string} token - Authentication token.
   * @param {string} id - ID of the kata to update.
   * @param {Object} kataData - New kata data.
   * @returns {Promise<any>} - Promise with the updated kata data.
   */
export const updateKata = (token: string, id: string, data: object) => {
    // http://localhost:8000/api/katas?id=XXXXXXXXXXXX
    // Add headers with JWT in x-access-token
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id
        }
    }

    return axios.put('/katas', data, options)
}


/**
   * Delete an existing kata.
   * @param {string} token - Authentication token.
   * @param {string} id - ID of the kata to delete.
   * @returns {Promise<any>} - Promise with the deleted kata data.
   */

export const deleteKata = (token: string, id: string) => {
    // http://localhost:8000/api/katas?id=XXXXXXXXXXXX
    // Add headers with JWT in x-access-token
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id
        }
    }

    return axios.delete('/katas', options)
}

/**
   * Sove an existing kata.
   * @param {string} token - Authentication token.
   * @param {string} id - ID of the kata to update.
   * @param {Object} kataData - New kata data.
   * @returns {Promise<any>} - Promise with the updated kata data.
   */
export const solveKata = (token: string, id: string, data: object) => {
    // http://localhost:8000/api/katas/solve?id=XXXXXXXXXXXX
    // Add headers with JWT in x-access-token
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id
        }
    }

    return axios.post('/katas/solve', data, options)
}

export const getMyKatas = (token:string, limit?: number, page?: number) => {
    // http://localhost:8000/api/katas/my-katas
    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            limit,
            page
        }
    }
    return axios.get('/katas/mykatas', options)
}
