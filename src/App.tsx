import React from 'react';
import './App.css';
import Routes from './routes/Routes';
import { StickyFooter } from './components/dashboard/StickyFooter';

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
