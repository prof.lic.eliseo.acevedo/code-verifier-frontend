
export enum KataLevel {
    Easy = 'Easy',
    Medium = 'Medium',
    Hard = 'Hard'
}

export type Kata = {
    _id: number,
    name: string,
    description: string,
    level: KataLevel,
    intents: number,
    stars: number,
    creator: string,
    solution: string,
    participants: {
        userId: string,
        intents: number
    }[],
    ratings: {
        userId: string,
        score: number
    }[],
    
}

export type FormValues = {
    name: string,
    description: string,
    level: KataLevel,
    solution: string,
    userSolution?: string;
}

