// routes/Routes.tsx
import React from 'react';
import { Routes as AppRoutes, Navigate, Route } from 'react-router-dom';
import HomePage from '../pages/HomePage';
import KatasDetailPage from '../pages/KatasDetailPage';
import KatasPages from '../pages/KatasPages';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import KatasCreatePage from '../pages/KatasCreatePage';
import KatasUpdatePage from '../pages/KatasUpdatePage';
import KatasDeletePage from '../pages/KatasDeletePage';
import KatasSolvePage from '../pages/KatasSolvePage';
import MyKatasPages from '../pages/MyKatasPages';

const Routes: React.FC = () => {
  return (
    <AppRoutes>
      <Route path="/" element={<HomePage />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/register" element={<RegisterPage />} />
      <Route path="/katas" element={<KatasPages />} />
      <Route path="/katas/mykatas" element={<MyKatasPages />} />
      <Route path="/katas/:id" element={<KatasDetailPage />} />
      <Route path='/katas/create' element={<KatasCreatePage />} />
      <Route path='/katas/update/:id' element={<KatasUpdatePage />} />
      <Route path='/katas/solve/:id' element={<KatasSolvePage />} />
      <Route path='/katas/delete/:id' element={<KatasDeletePage />} />
      {/* Redirecto when Page Not Found */}
      <Route 
            path='*' 
            element={<Navigate to='/' replace />}>
          </Route>

    </AppRoutes>
  );
};

export default Routes;
