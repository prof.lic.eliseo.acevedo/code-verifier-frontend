import React from 'react';
import { Link } from 'react-router-dom';

const Nav: React.FC = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/katas">Katas</Link>
        </li>
        <li>
          <Link to="/katas/create">Create Katas</Link>
        </li>
        <li>
          <Link to="/katas/update">Update Katas</Link>
        </li>
        <li>
          <Link to="/katas/delete">Delete Katas</Link>
        </li>
        <li>
          <Link to="/login">Login</Link>
        </li>
        <li>
          <Link to="/register">Register</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
