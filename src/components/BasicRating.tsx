import * as React from "react";
import Box from "@mui/material/Box";
import Rating from "@mui/material/Rating";
import Typography from "@mui/material/Typography";

interface BasicRatingProps {
  value: number;
}

const BasicRating = ({ value }: BasicRatingProps) => {
  const [ratingValue, setRatingValue] = React.useState<number | null>(value);

  return (
    <Box sx={{ '& > legend': { mt: 2 } }}>
      <Typography component="legend">Ranking score</Typography>
      <Rating name="read-only" value={ratingValue} readOnly />
    </Box>
  );
};

export default BasicRating;
