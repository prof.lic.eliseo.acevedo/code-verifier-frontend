import { Highlight } from 'prism-react-renderer';

interface EditorProps {
    language?: any,
    children?: any,
    soluction?: any
}

const Editor = ( { language, children,soluction  }: EditorProps ) => {

    return (
        <Highlight code={children} language='typescript' >
            {({ className, style, tokens, getLineProps, getTokenProps }) => (
                <pre className={className} style={style}>
                    {tokens.map((line, i) => (
                    <div {...getLineProps({ line, key: i })}>
                        {line.map((token, key) => (
                        <span {...getTokenProps({ token, key })} />
                        ))}
                    </div>
                    ))}
                </pre>
            )}
        </Highlight>
    );

}

export default Editor;