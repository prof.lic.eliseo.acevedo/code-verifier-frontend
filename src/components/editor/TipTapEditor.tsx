import React, { useEffect, useState } from 'react';
import { useEditor, EditorContent, ReactNodeViewRenderer } from '@tiptap/react';
import Document from '@tiptap/extension-document';
import Paragraph from '@tiptap/extension-paragraph';
import Text from '@tiptap/extension-text';
import CodeBlockLowlight from '@tiptap/extension-code-block-lowlight';
import { CodeBlock } from './CodeBlock';
import Box from '@mui/material/Box';

import './styles/mainStyles.scss';

// load all highlight.js languages
import { lowlight } from 'lowlight';


interface CharacterLimitProps {
  limit: number;
  content: string;
}

export const TipTapEditor = () => {
  const [content, setContent] = useState('// Add your code');
  const editor = useEditor({
    extensions: [
      Document,
      Paragraph,
      Text,
      CodeBlockLowlight
        .extend({
          addNodeView() {
            return ReactNodeViewRenderer(CodeBlock);
          },
        })
        .configure({ lowlight }),
    ],
    content: content,
    onUpdate: ({ editor }) => {
      setContent(editor.getHTML());
    },
  });

  useEffect(() => {
    editor?.chain().focus().toggleCodeBlock().run();
  }, [editor]);

  return (
    <Box>
      <EditorContent editor={editor} />
    </Box>
  );
};
