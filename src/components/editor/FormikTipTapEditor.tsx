import React, { useEffect } from 'react';
import { useEditor, EditorContent, ReactNodeViewRenderer } from '@tiptap/react';
import Document from '@tiptap/extension-document';
import Paragraph from '@tiptap/extension-paragraph';
import Text from '@tiptap/extension-text';
import Placeholder from '@tiptap/extension-placeholder';
import CodeBlockLowlight from '@tiptap/extension-code-block-lowlight';
import { CodeBlock } from './CodeBlock';
import Box from '@mui/material/Box';
import { useField } from 'formik';

import './styles/mainStyles.scss';

// load all highlight.js languages
import { lowlight } from 'lowlight';

interface FormikTipTapEditorProps {
  name: string;
}

const FormikTipTapEditor = ({ name }: FormikTipTapEditorProps) => {
  const [field, meta, helpers] = useField(name);

  const handleContentChange = (content: string) => {
    helpers.setValue(content);
  };

  return (
    <>
      <TipTapEditor
        content={field.value}
        onContentChange={handleContentChange}
        placeholder="Enter your text here"
      />
      {meta.touched && meta.error ? <div>{meta.error}</div> : null}
    </>
  );
};

interface TipTapEditorProps {
  content: string;
  onContentChange: (content: string) => void;
  placeholder?: string;
}

const TipTapEditor = ({
  content,
  onContentChange,
  placeholder,
}: TipTapEditorProps) => {
  const editor = useEditor({
    extensions: [
      Document,
      Paragraph,
      Text,
      Placeholder.configure({ placeholder }),
      CodeBlockLowlight.extend({
        addNodeView() {
          return ReactNodeViewRenderer(CodeBlock);
        },
      }).configure({ lowlight }),
    ],
    content,
    onUpdate() {
      onContentChange(editor?.getHTML() || '');
    },
  });

  useEffect(() => {
    editor?.chain().focus().toggleCodeBlock().run();
  }, [editor]);

  return (
    <Box>
      <EditorContent editor={editor} />
    </Box>
  );
};

export default FormikTipTapEditor;
