import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import DashboardIcon from "@mui/icons-material/Dashboard";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import HomeIcon from '@mui/icons-material/Home';


import MySearch from "./MySearch";

import { useNavigate } from 'react-router-dom';

const ButtonAppBar = () => {
  const navigate = useNavigate();
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>

          <Box>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={() => navigate('/')}
            >
              <HomeIcon />
              <Button color="inherit"> HOME </Button>
            </IconButton>
          </Box>

          <DashboardIcon />
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, marginLeft: '8px'  }}>
            Katas
          </Typography>

          <Box>
            <IconButton
              size="small"
              color="inherit"
              aria-label="read"
              onClick={() => navigate('/katas/mykatas')}
            >
              <LibraryBooksIcon />
              <Button color="inherit">Mis Katas </Button>
            </IconButton>

            <IconButton
              size="small"
              color="inherit"
              aria-label="create"
              sx={{ ml: 2 }}
              onClick={() => navigate('/katas/create')}
            >
              <AddCircleIcon />
              <Button color="inherit">Create</Button>
            </IconButton>
          </Box>
          <MySearch />

        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default ButtonAppBar;