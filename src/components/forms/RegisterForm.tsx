import React from 'react';

import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { AxiosResponse } from 'axios';

import { register } from '../../services/authService';

// Register Component
const RegisterForm = () => {
  // We define the initial credentials
  const initialCredentials = {
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
    age: 10
  }

  // Define Schema of validation with Yup
  const registerSchema = Yup.object().shape(
    {
      name: Yup.string()
        .min(6, 'Username must have 6 letters minimum')
        .max(12, 'Username must have maximum 12 letters')
        .required('Username is required'),
      email: Yup.string()
        .email('Invalid email format')
        .required('Email is required'),
      password: Yup.string()
        .min(8, 'Password too short')
        .required('Password is required'),
      confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), undefined], 'Passwords must match')
        .required('You mus confirm your password'),
      age: Yup.number()
        .min(10, 'You must be over 10 years old')
        .required('Age is required')
    }

  );


  return (
    <div>
      <h4>Register Form</h4>
      {/* Formik to encapsulate a Form */}
      <Formik
        initialValues={initialCredentials}
        validationSchema={registerSchema}
        onSubmit={async (values) => {
          // request to register endpoint
          register(values.name, values.email, values.password, values.age).then((response: AxiosResponse) => {
            if (response.status === 200) {
              console.table(values);
              alert('Registration successful');
            } else {
              throw new Error('Registration failed')
            }
          }).catch((error) => console.error(`[REGISTER ERROR]: Something went wrong: ${error}`))
        }
        }
      >
        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
            <Form>
              {/* Name Field */}
              <label htmlFor='name' >Nombre</label>
              <Field id='name' type='text' name='name' placeholder='nombre ' />

              {/* Name Errors */}
              {
                errors.name && touched.name && (
                  <ErrorMessage name='name' component='div'></ErrorMessage>
                )
              }

              {/* Email Field */}
              <label htmlFor='email' >Email</label>
              <Field id='email' type='email' name='email' placeholder='example@email.com' />

              {/* Email Errors */}
              {
                errors.email && touched.email && (
                  <ErrorMessage name='email' component='div'></ErrorMessage>
                )
              }

              {/* Password Field */}
              <label htmlFor='password' >Password</label>
              <Field id='password' type='password' name='password' placeholder='example' />
              {/* Password Errors */}
              {
                errors.password && touched.password && (
                  <ErrorMessage name='password' component='div'></ErrorMessage>
                )
              }

              {/* confirmPassword Field */}
              <label htmlFor='confirmPassword'>confirmPassword</label>
              <Field id='confirmPassword' type='password' name='confirmPassword' placeholder='example' />
              {/* confirmPassword Errors */}
              {errors.confirmPassword && touched.confirmPassword && (
                <ErrorMessage name='confirmPassword' component='div' />
              )}

              {/* Age Field */}
              <label htmlFor='age' >Edad</label>
              <Field id='age' type='number' name='age' />

              {/* Age Errors */}
              {
                errors.age && touched.age && (
                  <ErrorMessage name='age' component='div'></ErrorMessage>
                )
              }


              {/* SUBMIT FORM */}
              <button type='submit' disabled={isSubmitting || Object.keys(errors).length > 0} >Register</button>

              {/* Message if the form is submitting */}
              {
                isSubmitting ?
                  (<p>Seding data to register...</p>)
                  : null
              }

            </Form>
          )
        }
      </Formik>

    </div>
  )


}

export default RegisterForm;
