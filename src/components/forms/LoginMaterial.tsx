import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { AxiosResponse } from 'axios';
import { login } from '../../services/authService';
import { Link as RouterLink } from 'react-router-dom';

const loginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid Email Format').required('Email is required'),
  password: Yup.string().required('Password is required')
});

export default function LoginForm() {
  const navigate = useNavigate();

  const handleSubmit = async (values: any) => {
    try {
      const response = await login(values.email, values.password);
      if (response.status === 200) {
        if (response.data.token) {
          await sessionStorage.setItem('sessionJWTToken', response.data.token);
          navigate('/');
        } else {
          throw new Error('Error generating Login Token');
        }
      } else {
        throw new Error('Invalid Credentials');
      }
    } catch (error) {
      console.error(`[LOGIN ERROR]: Something went wrong: ${error}`);
    }
  };

  return (
    <ThemeProvider theme={createTheme()}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Formik
            initialValues={{
              email: '',
              password: ''
            }}
            validationSchema={loginSchema}
            onSubmit={handleSubmit}
          >
            {({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
              <Form>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={touched.email && Boolean(errors.email)}
                  helperText={touched.email && errors.email}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={touched.password && Boolean(errors.password)}
                  helperText={touched.password && errors.password}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Sign In
                </Button>

              </Form>
            )}
          </Formik>
          <Grid container>
                  <Grid item xs>
                    <Link href="#" variant="body2">
                      Forgot password?
                    </Link>
                  </Grid>
                  <Grid item>
                  <Link component={RouterLink} to="/register" variant="body2" >
                    {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
