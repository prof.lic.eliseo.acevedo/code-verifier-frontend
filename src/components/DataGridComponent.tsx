
import * as React from 'react';
import { useState, useEffect } from 'react';
import { AxiosResponse } from 'axios';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { DataGrid, GridColDef } from '@mui/x-data-grid';

import { useSessionStorage } from '../hooks/useSessionStorage';
import { Kata } from '../utils/types/Kata.type';
import IconButton from '@mui/material/IconButton';
import AddCircleIcon from "@mui/icons-material/AddCircle";
import Button from '@mui/material/Button';


interface DataGridComponentProps {
  getData: (token: string) => Promise<AxiosResponse>;
}

const DataGridComponent = ({ getData }: DataGridComponentProps) => {
  let loggedIn = useSessionStorage('sessionJWTToken');
  let navigate = useNavigate();
  const [katas, setKatas] = useState<Kata[]>([]);

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      getData(loggedIn)
        .then((response: AxiosResponse) => {
          if (
            response.status === 200 &&
            response.data.katas &&
            response.data.totalPages &&
            response.data.currentPage
          ) {
            let { katas } = response.data;
            setKatas(katas);
          } else {
            throw new Error(`Error obtaining katas: ${response.data}`);
          }
        })
        .catch((error) => console.error(`[Get All Katas Error] ${error}`));
    }
  }, [loggedIn, navigate, getData]);

const columns: GridColDef[] = [
      {
        field: 'detail',
        headerName: 'Detail',
        width: 80,
        renderCell: (params) => {
          const id = params.row._id;
          return (
            <IconButton
              size="small"
              color="inherit"
              aria-label="create"
              sx={{ ml: 2 }}
              onClick={() => navigate(`/katas/${id}`)}
            >
              <AddCircleIcon />
              {/* <Button color="inherit">Detail</Button> */}
            </IconButton>
          );
        },
      },
      { field: '_id', headerName: 'ID', width: 90 },
      {
        field: 'name',
        headerName: 'Name',
        width: 150,
      },
      {
        field: 'description',
        headerName: 'Description',
        width: 150,
      },
      {
        field: 'creator',
        headerName: 'Creator',
        width: 150,
      },
      {
        field: 'stars',
        headerName: 'Rating',
        width: 150,
      },
    ];
  
    return (
        <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          width: '100%',
        }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'center', mb: 2, mt:2 }}>
                <Typography variant="h4">Katas</Typography>
        </Box>
        <Box sx={{ height: 400, width: '90%'  }}>
        <DataGrid
          rows={katas}
          columns={columns}
          getRowId={(row) => row._id}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 5,
              },
            },
          }}
          pageSizeOptions={[5]}
          disableRowSelectionOnClick
        />
      </Box>
      </Box>
    );
};

export default DataGridComponent;
