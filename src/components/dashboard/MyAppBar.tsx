import * as React from 'react';
import { styled } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Badge from '@mui/material/Badge';
import NotificationsIcon from '@mui/icons-material/Notifications';
import LogoutIcon from '@mui/icons-material/Logout';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import HomeIcon from '@mui/icons-material/Home';

import { useNavigate } from 'react-router-dom';


interface MyAppBarProps {
  title: string;
}

const MyAppBar = ({ title }: MyAppBarProps) => {
  const navigate = useNavigate();

  return (
    <AppBar position='absolute' >
      <Toolbar sx={{ pr: '24px' }}>

        <Box>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => navigate('/')}
          >
            <HomeIcon />
            <Button color="inherit"> HOME </Button>
          </IconButton>
        </Box>


        <Typography
          component='h1'
          variant='h6'
          color='inherit'
          noWrap
          sx={{
            flexGrow: 1,
          }}
        >
          {title}
        </Typography>
        <IconButton color='inherit'>
          <Badge badgeContent={10} color='secondary'>
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <IconButton color='inherit'>
          <LogoutIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}

export default MyAppBar;