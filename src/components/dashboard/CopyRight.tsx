import { Typography, TypographyProps } from "@mui/material";
import Link from '@mui/material/Link'

export const Copyright = (props: TypographyProps) => {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://github.com/Eliseo2021">
                Michael's Repo
            </Link>{' '}
            {new Date().getFullYear()}
        </Typography>
    )
}
