import React from 'react';

type KataPreviewProps = {
  kataId: number;
  kataName: string;
  navigateToKataDetail: (id: number) => void;
};

const KataPreview: React.FC<KataPreviewProps> = ({ kataId, kataName, navigateToKataDetail }) => {
  return (
    <li onClick={() => navigateToKataDetail(kataId)}>
      {kataName} 
    </li>
  );
};

export default KataPreview;
