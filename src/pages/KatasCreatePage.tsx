import { AxiosResponse } from 'axios';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
//material UI
import {
    Box,
    Button,
    Container,
    CssBaseline,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    Stack,
    TextField,
    ThemeProvider,
    Typography,
    createTheme
} from '@mui/material';
//Editor TipTap 
// import { TipTapEditor } from '../components/editor/TipTapEditor';
import FormikTipTapEditor from '../components/editor/FormikTipTapEditor';

import MyAppBar from '../components/dashboard/MyAppBar';

// React Router DOM Imports
import { useNavigate } from 'react-router-dom';
import { useSessionStorage } from '../hooks/useSessionStorage';
import { createKata } from '../services/katasService';
import { Kata, KataLevel, FormValues } from '../utils/types/Kata.type';
import { FileUploader } from '../components/uploader/FileUploader';

const defaultTheme = createTheme();

const KatasCreatePage = () => {
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();


    const handleCreateKata = (values: FormValues) => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            // Add creator field to values object
            const kataData: Kata = {
                ...values,
                _id: 0,
                intents: 0,
                stars: 0,
                creator: loggedIn,
                participants: [],
                ratings:[]
            };
            createKata(loggedIn, kataData)
                .then((response: AxiosResponse) => {
                    if (response.status === 201) {
                        navigate('/katas');
                    }
                })
                .catch((error: any) => console.error(`[Create Kata ERROR]: ${error}`));
        }
    };

    const validationSchema = Yup.object({
        name: Yup.string()
            .required('Required'),
        description: Yup.string()
            .required('Required'),
        level: Yup.string()
            .oneOf(Object.values(KataLevel), 'Invalid level')
            .required('Required'),
        solution: Yup.string()
            .required('Required')
    });

    return (
        <>
        <ThemeProvider theme={defaultTheme}>
            <CssBaseline />
            <MyAppBar title={'Create Kata'} />

            <main>

                <Box
                    sx={{
                        bgcolor: 'background.paper',
                        pt: 8,
                        pb: 6,
                    }}
                >
                    <Container maxWidth="sm" >
                        <Typography
                            component="h1"
                            variant="h2"
                            align="center"
                            color="text.primary"
                            gutterBottom
                            sx={{ mt: 3 }}
                        >
                            Create Kata
                        </Typography>
                    </Container>
                </Box>

        <Container maxWidth="sm">
            <Box sx={{ mt: 1 }}>
                <Formik<FormValues>
                    initialValues={{
                        name: '',
                        description: '',
                        level: KataLevel.Easy,
                        solution: ''
                    }}
                    validationSchema={validationSchema}
                    onSubmit={handleCreateKata}
                >
                    {({ errors, touched }) => (
                        <Form>
                            <Box sx={{ mb: 2 }}>
                                <Field name="name">
                                    {({ field }: any) => (
                                        <TextField
                                            fullWidth
                                            label="Name"
                                            {...field}
                                            error={touched.name && Boolean(errors.name)}
                                        />
                                    )}
                                </Field>
                                <ErrorMessage name="name">
                                    {(msg) => (
                                        <FormHelperText error>{msg}</FormHelperText>
                                    )}
                                </ErrorMessage>
                            </Box>

                            <Box sx={{ mb: 2 }}>
                                <Field name="description">
                                    {({ field }: any) => (
                                        <TextField
                                            fullWidth
                                            multiline
                                            rows={4}
                                            label="Description"
                                            {...field}
                                            error={touched.description && Boolean(errors.description)}
                                        />
                                    )}
                                </Field>
                                <ErrorMessage name="description">
                                    {(msg) => (
                                        <FormHelperText error>{msg}</FormHelperText>
                                    )}
                                </ErrorMessage>
                            </Box>

                            <Box sx={{ mb: 2 }}>
                                <FormControl fullWidth error={touched.level && Boolean(errors.level)}>
                                    <InputLabel htmlFor="level" sx={{ mb: 4 }}>Level</InputLabel>
                                    <Field id="level" name="level" as={Select} >
                                        {Object.values(KataLevel).map(level => (
                                            <MenuItem key={level} value={level}>{level}</MenuItem>
                                        ))}
                                    </Field>
                                    <ErrorMessage name="level">
                                        {(msg) => (
                                            <FormHelperText error>{msg}</FormHelperText>
                                        )}
                                    </ErrorMessage>
                                </FormControl>
                            </Box>

                            <Box sx={{ mb: 2 }}>
                                <Field name="solution">
                                    {({ field, form }: any) => (
                                        <>
                                            <label htmlFor={field.name}>Solucion</label>
                                            <FormikTipTapEditor name="solution" />
                                            <ErrorMessage name="solution">
                                                {(msg) => <FormHelperText error>{msg}</FormHelperText>}
                                            </ErrorMessage>
                                        </>
                                    )}
                                </Field>
                                <ErrorMessage name="solution">
                                    {(msg) => (
                                        <FormHelperText error>{msg}</FormHelperText>
                                    )}
                                </ErrorMessage>
                            </Box>

                            <Button type="submit" variant="contained" sx={{ mb: 2 }}>
                                Create Kata
                            </Button>
                        </Form>
                    )}
                </Formik>
            </Box>
            <Box>
            <FileUploader token={loggedIn}/>
            </Box>
        </Container>
        </main>
            </ThemeProvider>

        </>
    );
};

export default KatasCreatePage;
