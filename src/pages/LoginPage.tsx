import React from 'react'
import LoginForm from '../components/forms/LoginMaterial'

const LoginPage = () => {
  return (
    <>
    <LoginForm />
    </>
  )
}

export default LoginPage