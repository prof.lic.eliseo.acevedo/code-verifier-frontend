import { AxiosResponse } from 'axios';
import { useEffect, useState } from 'react';

// React Router DOM Imports
import { useNavigate, useParams } from 'react-router-dom';
import Editor from '../components/editor/Editor';
import { useSessionStorage } from '../hooks/useSessionStorage';
import { getKataByID } from '../services/katasService';
import { Kata } from '../utils/types/Kata.type';

//MaterialUI Import
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import BasicRating from '../components/BasicRating';
import MyAppBar from '../components/dashboard/MyAppBar';

const defaultTheme = createTheme();

const KatasDetailPage = () => {
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    // Find id from params
    let { id } = useParams();

    const [kata, setKata] = useState<Kata | undefined>(undefined);
    

    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            if (id) {
                getKataByID(loggedIn, id).then((response: AxiosResponse) => {
                    if (response.status === 200 && response.data) {
                        let { __v, ...kataData } = response.data;
                        setKata(kataData);
                    }
                }).catch((error: any) => console.error(`[Kata By ID ERROR]: ${error}`))
            } else {
                return navigate('/katas');
            }
        }

    }, [loggedIn, id, navigate]);


    return (
        <>
            <ThemeProvider theme={defaultTheme}>
                <CssBaseline />
                <MyAppBar title={'Kata Detail Page'} />

                <main>
                    <Box
                        sx={{
                            bgcolor: 'background.paper',
                            pt: 8,
                            pb: 6,
                        }}
                    >
                        <Container maxWidth="sm">
                            <Typography
                                component="h1"
                                variant="h2"
                                align="center"
                                color="text.primary"
                                gutterBottom
                            >
                                Kata Detail Page
                            </Typography>
                            <Typography variant="h5" align="center" color="text.secondary" paragraph>
                                Something short and leading about the collection below—its contents,
                                the creator.
                            </Typography>
                            <Stack
                                sx={{ pt: 4 }}
                                direction="row"
                                spacing={2}
                                justifyContent="center"
                            >
                                <Button onClick={() => (navigate(`/katas/solve/${kata?._id}`))} variant="contained">Solve Kata</Button>
                                <Button onClick={() => (navigate('/'))} variant="contained">score Kata</Button>
                                <Button onClick={() => (navigate(`/katas/update/${kata?._id}`))} variant="outlined">Edit Kata</Button>
                                <Button onClick={() => navigate(`/katas/delete/${kata?._id}`)} variant="contained" style={{ backgroundColor: 'red', color: '#fff' }}>
                                    Delete Kata
                                </Button>

                            </Stack>
                        </Container>
                    </Box>

                    <Container sx={{ py: 8, display: 'flex', justifyContent: 'center' }} maxWidth="md">
                        <Grid container spacing={1}>
                            <Grid item xs={12} sm={10} md={8}>
                                <Card
                                    sx={{ width: '100%', height: '400px', display: 'flex', flexDirection: 'column' }}
                                >
                                    {kata ? (
                                        <>
                                            <CardContent sx={{ flexGrow: 1 }}>
                                                <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                                    <Typography gutterBottom variant="h5" component="h2">
                                                        Name Kata: <strong>{kata?.name}</strong>
                                                        <Typography variant="body1">ID: {kata._id}</Typography>
                                                    </Typography>
                                                    <BasicRating value={kata.stars} />
                                                </Box>

                                                <Typography sx={{ marginBottom: '2rem' }}>
                                                    This is a media card. Use this section to describe the content.
                                                </Typography>
                                                <Box sx={{ mb: 2, height: '100px', border: '1px solid grey', borderRadius: '5px' }}>
                                                    <Typography variant="body1" style={{ wordWrap: 'break-word' }}>{kata.description}</Typography>
                                                </Box>

                                            </CardContent>
                                        </>
                                    ) : (
                                        <CardContent sx={{ flexGrow: 1 }}>
                                            <Typography>Loading data...</Typography>
                                        </CardContent>
                                    )}
                                    <CardActions>
                                        <Button onClick={() => (navigate(`/katas/solve/${kata?._id}`))} size="small">Solve Kata</Button>
                                        <Button onClick={() => (navigate('/'))} size="small">Back to Kata</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        </Grid>
                    </Container>

                </main>
            </ThemeProvider>

        </>
    )
}

export default KatasDetailPage;