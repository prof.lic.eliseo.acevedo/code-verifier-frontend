import { Dashboard } from '../components/dashboard/Dashboard'
import { StickyFooter } from '../components/dashboard/StickyFooter'

const HomePage = () => {
  return (
    <>
    <Dashboard />
    <StickyFooter />
    </>
  )
}

export default HomePage