import React from 'react'
import RegisterForm from '../components/forms/RegisterMaterial'

const RegisterPage = () => {
  return (
    <>
    <RegisterForm />
    </>
  )
}

export default RegisterPage