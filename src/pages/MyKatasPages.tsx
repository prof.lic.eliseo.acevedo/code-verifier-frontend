import React from 'react'
import DataGridComponent from '../components/DataGridComponent'
import ButtonAppBar from '../components/ButtonAppBar'
import { getMyKatas } from '../services/katasService'

const MyKatasPages = () => {
  return (
    <>
    <ButtonAppBar />
    <DataGridComponent getData={getMyKatas} />

    </>
  )
}

export default MyKatasPages