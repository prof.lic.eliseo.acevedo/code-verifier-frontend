import { AxiosResponse } from 'axios';
import { useState, useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
//material UI
import {
    Box,
    Button,
    CardActions,
    Container,
    CssBaseline,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    Stack,
    TextField,
    ThemeProvider,
    Typography,
    createTheme
} from '@mui/material';
//Editor TipTap 
// import { TipTapEditor } from '../components/editor/TipTapEditor';
import FormikTipTapEditor from '../components/editor/FormikTipTapEditor';

import MyAppBar from '../components/dashboard/MyAppBar';


// React Router DOM Imports
import { useNavigate, useParams } from 'react-router-dom';
import { useSessionStorage } from '../hooks/useSessionStorage';
import { updateKata, getKataByID } from '../services/katasService';
import { Kata, KataLevel, FormValues } from '../utils/types/Kata.type';

const defaultTheme = createTheme();

const KatasUpdatePage = () => {
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    let { id } = useParams();

    const [kataData, setKataData] = useState<Kata | undefined>(undefined);

    useEffect(() => {
        if (id) {
            getKataByID(loggedIn, id)
                .then((response: AxiosResponse) => {
                    if (response.status === 200) {
                        setKataData(response.data);
                    }
                })
                .catch((error: any) => console.error(`[Get Kata ERROR]: ${error}`));
        }
    }, [id, loggedIn]);

    const handleUpdateKata = (values: FormValues) => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            if (id && kataData) {
                // Add creator field to values object
                const updatedKataData: Kata = {
                    ...kataData,
                    ...values
                };
                updateKata(loggedIn, id, updatedKataData)
                    .then((response: AxiosResponse) => {
                        if (response.status === 200) {
                            navigate('/katas');
                        }
                    })
                    .catch((error: any) => console.error(`[Update Kata ERROR]: ${error}`));
            }
        }
    };

    const validationSchema = Yup.object({
        name: Yup.string()
            .required('Required'),
        description: Yup.string()
            .required('Required'),
        level: Yup.string()
            .oneOf(Object.values(KataLevel), 'Invalid level')
            .required('Required'),
        solution: Yup.string()
            .required('Required')
    });

    return (
        <>
            <ThemeProvider theme={defaultTheme}>
                <CssBaseline />
                <MyAppBar title={'Update Kata'} />

                <main>

                    <Box
                        sx={{
                            bgcolor: 'background.paper',
                            pt: 8,
                            pb: 6,
                        }}
                    >
                        <Container maxWidth="sm" >
                            <Typography
                                component="h1"
                                variant="h2"
                                align="center"
                                color="text.primary"
                                gutterBottom
                                sx={{ mt: 3 }}
                            >
                                Updata Kata
                            </Typography>
                        </Container>
                    </Box>

                    <Container maxWidth="sm">
                        <Box sx={{ mt: 1 }}>

                            {kataData && (
                                <Formik<FormValues>
                                    initialValues={{
                                        name: kataData.name,
                                        description: kataData.description,
                                        level: kataData.level,
                                        solution: kataData.solution
                                    }}
                                    validationSchema={validationSchema}
                                    onSubmit={handleUpdateKata}
                                >
                                    {({ errors, touched }) => (
                                        <Form>
                                            <Box sx={{ mb: 2 }}>
                                                <Field name="name">
                                                    {({ field }: any) => (
                                                        <TextField
                                                            fullWidth
                                                            label="Name"
                                                            {...field}
                                                            error={touched.name && Boolean(errors.name)}
                                                        />
                                                    )}
                                                </Field>
                                                <ErrorMessage name="name">
                                                    {(msg) => (
                                                        <FormHelperText error>{msg}</FormHelperText>
                                                    )}
                                                </ErrorMessage>
                                            </Box>

                                            <Box sx={{ mb: 2 }}>
                                                <Field name="description">
                                                    {({ field }: any) => (
                                                        <TextField
                                                            fullWidth
                                                            multiline
                                                            rows={4}
                                                            label="Description"
                                                            {...field}
                                                            error={touched.description && Boolean(errors.description)}
                                                        />
                                                    )}
                                                </Field>
                                                <ErrorMessage name="description">
                                                    {(msg) => (
                                                        <FormHelperText error>{msg}</FormHelperText>
                                                    )}
                                                </ErrorMessage>
                                            </Box>

                                            <Box sx={{ mb: 2 }}>
                                                <FormControl fullWidth error={touched.level && Boolean(errors.level)}>
                                                    <InputLabel htmlFor="level" sx={{ mb: 4 }}>Level</InputLabel>
                                                    <Field id="level" name="level" as={Select}>
                                                        {Object.values(KataLevel).map(level => (
                                                            <MenuItem key={level} value={level}>{level}</MenuItem>
                                                        ))}
                                                    </Field>
                                                    <ErrorMessage name="level">
                                                        {(msg) => (
                                                            <FormHelperText error>{msg}</FormHelperText>
                                                        )}
                                                    </ErrorMessage>
                                                </FormControl>
                                            </Box>

                                            <Box sx={{ mb: 2 }}>
                                                <Field name="solution">
                                                    {({ field, form }: any) => (
                                                        <>
                                                            <label htmlFor={field.name}>Solucion</label>
                                                            <FormikTipTapEditor name="solution" />
                                                            <ErrorMessage name="solution">
                                                                {(msg) => <FormHelperText error>{msg}</FormHelperText>}
                                                            </ErrorMessage>
                                                        </>
                                                    )}
                                                </Field>
                                            </Box>

                                            <CardActions>
                                                <Button type="submit" variant="contained">Update Kata</Button>
                                                <Button onClick={() => (navigate('/'))} size="small">Back to Katas</Button>
                                            </CardActions>
                                        </Form>
                                    )}
                                </Formik>
                            )}

                        </Box>
                    </Container>
                </main>
            </ThemeProvider>
        </>
    );
};

export default KatasUpdatePage;
