import React from 'react'
import DataGridComponent from '../components/DataGridComponent'
import ButtonAppBar from '../components/ButtonAppBar'
import { getAllKatas } from '../services/katasService'

const KatasPages = () => {
  return (
    <>
    <ButtonAppBar />
    <DataGridComponent getData={getAllKatas} />

    </>
  )
}

export default KatasPages