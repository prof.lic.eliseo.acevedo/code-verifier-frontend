import { AxiosResponse } from 'axios';
import { useState, useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
//material UI
import {
    Box,
    Button,
    CardActions,
    CardContent,
    Container,
    CssBaseline,
    FormControl,
    FormHelperText,
    InputLabel,
    MenuItem,
    Select,
    Stack,
    TextField,
    ThemeProvider,
    Typography,
    createTheme
} from '@mui/material';
//Editor TipTap 
// import { TipTapEditor } from '../components/editor/TipTapEditor';
import FormikTipTapEditor from '../components/editor/FormikTipTapEditor';

import MyAppBar from '../components/dashboard/MyAppBar';


// React Router DOM Imports
import { useNavigate, useParams } from 'react-router-dom';
import { useSessionStorage } from '../hooks/useSessionStorage';
import { solveKata, getKataByID } from '../services/katasService';
import { Kata, KataLevel, FormValues } from '../utils/types/Kata.type';
import BasicRating from '../components/BasicRating';

const defaultTheme = createTheme();

const KatasSolvePage = () => {
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    let { id } = useParams();

    const [showSolution, setShowSolution] = useState(false)
    const [kataData, setKataData] = useState<Kata | undefined>(undefined);
    const participant = kataData?.participants.find(p => p.userId === loggedIn);
    console.log(participant)
    useEffect(() => {
        if (id) {
            getKataByID(loggedIn, id)
                .then((response: AxiosResponse) => {
                    if (response.status === 200) {
                        setKataData(response.data);
                    }
                })
                .catch((error: any) => console.error(`[Get Kata ERROR]: ${error}`));
        }
    }, [id, loggedIn]);

    const handleSolveKata = (values: any) => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            if (id && kataData) {
                // Add creator field to values object
                const data = {
                    userSolution: values.userSolution
                };
                console.log(data);
                solveKata(loggedIn, id, data)
                    .then((response: AxiosResponse) => {
                        if (response.status === 200) {
                            // Check if userSolution is correct
                            if (response.data.message === 'Correct userSolution!') {
                                // Show congratulation message to user
                                alert('¡Felicitaciones! Has resuelto la kata correctamente.');
                            } else {
                                // Show encouragement message to user
                                alert('¡Sigue intentándolo! Estoy seguro de que puedes resolver esta kata.');
                            }
                        }
                    })
                    .catch((error: any) => console.error(`[Solve Kata ERROR]: ${error}`));
            }
        }
    };

    const validationSchema = Yup.object({
        userSolution: Yup.string()
            .required('Required')
    });




    return (
        <>
            <ThemeProvider theme={defaultTheme}>
                <CssBaseline />
                <MyAppBar title={'Solve Kata'} />

                <main>

                    <Box
                        sx={{
                            bgcolor: 'background.paper',
                            pt: 8,
                            pb: 6,
                        }}
                    >
                        <Container maxWidth="sm" >
                            <Typography
                                component="h1"
                                variant="h2"
                                align="center"
                                color="text.primary"
                                gutterBottom
                                sx={{ mt: 3 }}
                            >
                                Solve Kata
                            </Typography>
                        </Container>
                    </Box>

                    <Container maxWidth="sm">
                        <Box sx={{ mt: 1 }}>

                            {kataData && (
                                <Formik<FormValues>
                                    initialValues={{
                                        name: kataData.name,
                                        description: kataData.description,
                                        level: kataData.level,
                                        solution: kataData.solution,
                                        userSolution: 'enter your userSolution'
                                    }}
                                    validationSchema={validationSchema}
                                    onSubmit={handleSolveKata}
                                >
                                    {({ errors, touched }) => (
                                        <Form>

                                            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                                <Typography gutterBottom variant="h5" component="h2">
                                                    Name Kata: <strong>{kataData?.name}</strong>
                                                    <Typography variant="body1">ID: {kataData._id}</Typography>
                                                </Typography>
                                                <BasicRating value={kataData.stars} />
                                            </Box>
                                            <Box sx={{ mb: 2, height: '100px', border: '1px solid grey', borderRadius: '5px' }}>
                                                <Typography variant="body1" style={{ wordWrap: 'break-word' }}>{kataData.description}</Typography>
                                            </Box>

                                            <Typography variant="h5">
                                                Intens: {participant ? participant.intents : '0'}
                                            </Typography>

                                            <CardActions>
                                                <Button size="small" onClick={() => setShowSolution(!showSolution)}>
                                                    {showSolution ? 'Hide Solution' : 'Show Solution?'}
                                                </Button>
                                            </CardActions>
                                            {showSolution && (
                                                <FormikTipTapEditor name="solution" />
                                            )}

                                            <Box sx={{ mb: 2 }}>
                                                <Field name="userSolution">
                                                    {({ field, form }: any) => (
                                                        <>
                                                            <label htmlFor={field.name}>Solucion</label>
                                                            <FormikTipTapEditor name="userSolution" />
                                                            <ErrorMessage name="userSolution">
                                                                {(msg) => <FormHelperText error>{msg}</FormHelperText>}
                                                            </ErrorMessage>
                                                        </>
                                                    )}
                                                </Field>
                                                <ErrorMessage name="userSolution">
                                                    {(msg) => (
                                                        <FormHelperText error>{msg}</FormHelperText>
                                                    )}
                                                </ErrorMessage>
                                            </Box>

                                            <CardActions>
                                                <Button type="submit" variant="contained">Solve Kata</Button>
                                                <Button onClick={() => (navigate('/'))} size="small">Back to Katas</Button>
                                            </CardActions>
                                        </Form>
                                    )}
                                </Formik>
                            )}

                        </Box>
                    </Container>
                </main>
            </ThemeProvider>
        </>
    );
};

export default KatasSolvePage;
