import { AxiosResponse } from 'axios';
import { useState, useEffect } from 'react';

//MaterialUI
import { Box, Button, Card, CardActionArea, CardActions, CardContent, Container, CssBaseline, Grid, Stack, ThemeProvider, Typography, createTheme } from '@mui/material';

// React Router DOM Imports
import { useNavigate, useParams } from 'react-router-dom';
import { useSessionStorage } from '../hooks/useSessionStorage';
import { deleteKata, getKataByID } from '../services/katasService';
import { Kata } from '../utils/types/Kata.type';
import MyAppBar from '../components/dashboard/MyAppBar';

const defaultTheme = createTheme();

const KatasDeletePage = () => {
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    let { id } = useParams();

    const [kataData, setKataData] = useState<Kata | undefined>(undefined);

    useEffect(() => {
        if (id) {
            getKataByID(loggedIn, id)
                .then((response: AxiosResponse) => {
                    if (response.status === 200) {
                        setKataData(response.data);
                    }
                })
                .catch((error: any) => console.error(`[Get Kata ERROR]: ${error}`));
        }
    }, [id, loggedIn]);

    const handleDeleteKata = () => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            if (id && kataData) {
                deleteKata(loggedIn, id)
                    .then((response: AxiosResponse) => {
                        if (response.status === 200) {
                            navigate('/katas');
                        }
                    })
                    .catch((error: any) => console.error(`[Delete Kata ERROR]: ${error}`));
            }
        }
    };

    return (
        <>
            <ThemeProvider theme={defaultTheme}>
                <CssBaseline />
                <MyAppBar title={'Kata Delete '} />

                <main>
                    <Box
                        sx={{
                            bgcolor: 'background.paper',
                            pt: 8,
                            pb: 6,
                        }}
                    >
                        <Container maxWidth="sm">
                            <Typography
                                component="h1"
                                variant="h2"
                                align="center"
                                color="text.primary"
                                gutterBottom
                                sx={{ mt: 4 }}
                            >
                                Kata Delete
                            </Typography>
                        </Container>
                    </Box>



                    <Container sx={{ py: 8, display: 'flex', justifyContent: 'center' }} maxWidth="md">
                        <Grid container spacing={1}>
                            <Grid item xs={12} sm={10} md={8} sx={{ display: 'flex', justifyContent: 'center' }}>

                                <Card sx={{ width: '100%', height: '400px', p:2, alignContent: 'center' }}>
                                    <CardActionArea>
                                        <CardContent>
                                        {kataData && (
                                                <>
                                                    <Typography gutterBottom variant="h4"> Are you sure you want to delete the kata <strong>"{kataData.name}"</strong>?</Typography>
                                                </>
                                            )}
                                        </CardContent>
                                    </CardActionArea>
                                    <CardActions>
                                            <Button onClick={handleDeleteKata} variant="contained" sx={{ backgroundColor: 'red', color: 'white' }}>Yes, delete</Button>
                                            <Button onClick={() => (navigate('/katas'))} size="small">NO, Back to Kata</Button>
                                        
                                    </CardActions>
                                </Card>

                            </Grid>
                        </Grid>
                    </Container>

                </main>
            </ThemeProvider>

        </>
    );
};

export default KatasDeletePage;
